<?php

namespace App\Controller;

use App\Entity\Autos;
use App\Form\AutosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AutosController extends AbstractController
{
    /**
     * @Route("/autos", name="autos")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        if($user){
        $auto = new Autos();
        $from = $this->createForm(AutosType::class, $auto);
        $from->handleRequest($request);
        if ($from->isSubmitted() && $from->isValid()){
            $user = $this->getUser();
            $auto->setUser($user);
            $en = $this->getDoctrine()->getManager();
            $en->persist($auto);
            $en->flush();
            return $this->redirectToRoute('inicio');
        }
        return $this->render('autos/index.html.twig', [
            'controller_name' => 'Registrar un nuevo Auto',
            'formularioautos' => $from->createView()
        ]);
        }else{
            return $this->redirectToRoute('app_login');
        }
    }
}
