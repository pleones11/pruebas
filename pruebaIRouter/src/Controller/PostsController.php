<?php

namespace App\Controller;

use App\Entity\Autos;
use App\Entity\Posts;
use App\Form\AutosType;
use App\Form\PostsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostsController extends AbstractController
{
    /**
     * @Route("/registrar_posts", name="posts")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        if($user){
            $posts = new Posts();
            $from = $this->createForm(PostsType::class, $posts);
            $from->handleRequest($request);
            if ($from->isSubmitted() && $from->isValid()){
                $user = $this->getUser();
                $posts->setUser($user);
                $en = $this->getDoctrine()->getManager();
                $en->persist($posts);
                $en->flush();
                return $this->redirectToRoute('inicio');
            }
            return $this->render('posts/index.html.twig', [
                'controller_name' => 'CREAR UNA NUEVA PUBLICACION',
                'formularioposts' => $from->createView()
            ]);
        }else{
            return $this->redirectToRoute('app_login');
        }

    }
}
