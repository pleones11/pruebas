<?php

namespace App\Controller;

use App\Entity\Autos;
use App\Entity\Posts;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InicioController extends AbstractController
{
    /**
     * @Route("/inicio", name="inicio")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $user = $this->getUser();
        if($user){
            $en = $this->getDoctrine()->getManager();
            $query = $en->getRepository(Posts::class)->BuscarPosts();
            $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                2 /*limit per page*/
            );
            return $this->render('inicio/index.html.twig', [
                'controller_name' => 'Bienvenido al inicio del sitio',
                'pagination' => $pagination
            ]);
        }else{
            return $this->redirectToRoute('app_login');
        }
    }
}
