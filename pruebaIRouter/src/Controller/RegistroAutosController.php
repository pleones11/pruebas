<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;

use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistroAutosController extends AbstractController
{
    /**
     * @Route("/registro/autos", name="registro_autos")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $from = $this->createForm(UserType::class, $user);
        $from->handleRequest($request);
        if ($from->isSubmitted() && $from->isValid()){
            $en = $this->getDoctrine()->getManager();
            $user->setActive(true);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($passwordEncoder->encodePassword($user,$from['password']->getData()));
            $en->persist($user);
            $en->flush();
            $this->addFlash('exito',User::REGISTRO_EXITOSO);
            return $this->redirectToRoute('registro/autos');
        }
        return $this->render('registro_autos/index.html.twig', [
            'controller_name' => 'Registre un nuevo Auto',
            'formulario' => $from->createView()
        ]);
    }
}
