# PRUEBAS PRACTICA
LA PRUEBA PRACTICA SE REALIZO EN SYMFONY 5 (PHP), SE SIGUIERON LOS SIGUIENTES PASOS:

# Comandos Basicos de Symfony CLI:

- Crear nuevos proyectos: symfony new nombre-proyecto --full

- Ejecutar el proyecto: symfony server:start
 
- Crear la base de datos: php bin/console doctrine:database:create
 
- Crear tablas en base de datos: php bin/console make:entity
 
    - user
    - posts
    - comentarios

- Actualizar esquema de DB: php bin/console doctrine:schema:update --force
    - Crear usuarios: php bin/console make:user

- Para crear un nuevo controlador: php bin/console make:controller
 
- Para crear formularios: php bin/console make:form
 
- Para crear los formularios de login: php bin/console make:auth

# Resultados:
PAGINA PRINCIPAL
![ScreenShot](Captura_de_pantalla__43_.png)
AÑADIR PUBLICACION
![ScreenShot](Captura_de_pantalla__44_.png)

